PLOT REPORT
-----------

Report File:        Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym (PCB - PLOT REPORT).txt
Report Written:     Friday, August 05, 2016
Design Path:        Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym.pcb
Design Title:       
Created:            31.07.2016 12:39:04
Last Saved:         05.08.2016 23:55:08
Editing Time:       797 min
Units:              mm (precision 1)


Device Settings
===============


Gerber Settings
===============

    Leading zero suppression.
    G01 assumed throughout.
    Line termination <*> <CR> <LF>.
    3.5 format absolute inches.
    Format commands defined in Gerber file.
    Aperture table defined in Gerber file.
    Hardware arcs allowed.


Drill Settings
==============

    Excellon Format 1    Format 3.5 absolute in mm.
    No zero suppression.


Plots Output
============


Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Top Documentation.gbr

    Layers:
    ---------------------
    Top Documentation

    D-Code Shape  (thou)
    -----------------------

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Top Silkscreen.gbr

    Layers:
    ------------------
    Top Silkscreen

    D-Code Shape  (thou)
    -----------------------
    D11    Round 5.00
    D12    Round 8.00
    D13    Round 10.00

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Top Copper.gbr

    Layers:
    --------------
    Top Copper

    D-Code Shape  (thou)
    -----------------------------
    D11    Round 5.00
    D13    Round 10.00
    D14    Round 100.00
    D18    Round 118.11
    D19    Round 60.00
    D70    Oval 60.00 X 110.00
    D21    Round 48.00
    D22    Round 31.50
    D23    Round 62.99
    D88    Square 60.00
    D89    Round 60.00
    D98    Round 196.85

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Top Copper (Resist).gbr

    Layers:
    --------------
    Top Copper

    D-Code Shape  (thou)
    -----------------------------
    D27    Round 110.00
    D73    Round 128.11
    D74    Round 70.00
    D75    Oval 70.00 X 120.00
    D99    Round 206.85
    D100   Round 72.99
    D101   Square 70.00
    D102   Round 70.00

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Top Copper (Paste).gbr

    Layers:
    --------------
    Top Copper

    D-Code Shape  (thou)
    -----------------------

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Bottom Copper.gbr

    Layers:
    -----------------
    Bottom Copper

    D-Code Shape  (thou)
    -----------------------------
    D11    Round 5.00
    D13    Round 10.00
    D14    Round 100.00
    D15    Round 59.06
    D18    Round 118.11
    D19    Round 60.00
    D70    Oval 60.00 X 110.00
    D21    Round 48.00
    D22    Round 31.50
    D23    Round 62.99
    D88    Square 60.00
    D89    Round 60.00
    D98    Round 196.85

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Bottom Copper (Resist).gbr

    Layers:
    -----------------
    Bottom Copper

    D-Code Shape  (thou)
    -----------------------------
    D27    Round 110.00
    D73    Round 128.11
    D74    Round 70.00
    D75    Oval 70.00 X 120.00
    D99    Round 206.85
    D100   Round 72.99
    D101   Square 70.00
    D102   Round 70.00

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Bottom Copper (Paste).gbr

    Layers:
    -----------------
    Bottom Copper

    D-Code Shape  (thou)
    -----------------------

NC Drill Output
===============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Drill Data - [Through Hole].drl

    Output:
    ---------------------------
    Plated Round Drill Holes
    Plated Slots
    Plated Board Outlines

    Tool  Size         Count   ID
    ------------------------------------
    T001  000.02400 in 6     M
    T002  000.03000 in 9     L
    T003  000.03200 in 66    F
    T004  000.03500 in 16    A
    T005  000.06299 in 9     N
    ------------------------------------
    Total              106
    ------------------------------------

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Drill Data - [Through Hole] (Unplated).drl

    Output:
    -----------------------------
    Unplated Round Drill Holes
    Unplated Slots
    Unplated Board Outlines

    Tool  Size         Count   ID
    ------------------------------------
    T001  000.02500 in 1
    T002  000.03150 in 3     AC
    T003  000.12598 in 4     V
    ------------------------------------
    Total              8
    ------------------------------------


Gerber Output
=============

Z:\Serge\My Documents\Electronique\GU19PP\DesignSpark\CDiff\CDiff Sym - Drill Ident Drawing - [Through Hole].gbr

    Layers:
    ------------------
    [Through Hole]


Drill Ident Drawing
===================

    Drill Plated Size Shape ID
    -----------------------------------
    35    Y      100  Round A
    32    Y      100  Round F
    30    Y      100  Round L
    24    Y      100  Round M
    63    Y      100  Round N
    126          100  Round V
    31           100  Round AC

    D-Code Shape  (thou)
    -----------------------
    D11    Round 5.00
    D14    Round 100.00

End Of Report.

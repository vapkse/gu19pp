Design Rule Check Report
------------------------

Report File:        Z:\Serge\My Documents\Electronique\6E5P 5PP\Designspark\Output\Output (PCB - Design Rule Check Report).txt
Report Written:     Saturday, February 11, 2017
Design Path:        Z:\Serge\My Documents\Electronique\6E5P 5PP\Designspark\Output\Output.pcb
Design Title:       
Created:            11.02.2017 03:22:42
Last Saved:         11.02.2017 12:57:16
Editing Time:       104 min
Units:              mm (precision 1)


Results
=======


No errors found


Settings
========


Spacings

=========

Tracks        Yes
Pads and Vias Yes
Shapes        Yes
Text          Yes
Board         Yes
Drills        Yes
Components    No


Manufacturing

==============

Drill Breakout                  No
Drill Backoff                   No
Silkscreen Overlap              No
Copper Text In Board            No
Min Track Width                 No
Min Annular Ring                No
Min Paste Size                  No
Vias In Pads                    No
Unplated Vias                   No
Unplated Pads With Inner Tracks No


Nets

=====

Net Completion               Yes
Dangling Tracks              Yes
Net Track Length Differences No



End Of Report.

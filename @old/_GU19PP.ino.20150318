/*
  GU19 PP
 Version 1.00
 Date: 01.03.2015
 */

#include <SimpleTimer.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <DallasTemperature.h>

static const byte PPGU19_ID = 19;
static const byte ampId = PPGU19_ID;

// Pin Config
#define regD4Pin		        2
#define regD3Pin		        3
#define oneWireBusPin                   4
#define regF3Pin		        5
#define regF4Pin		        6
#define regD2Pin		        7
#define regD1Pin		        8
#define regF2Pin		        9
#define regF1Pin		        10
#define indicatorPin                    11
#define relayPin                        12
#define ledPin                          13
#define buttonPin	                53
#define currentD4Pin	                A0    
#define currentD3Pin	                A1    
#define currentF4Pin	                A2    
#define currentF3Pin	                A3    
#define currentD2Pin	                A4    
#define currentD1Pin	                A5    
#define currentF1Pin	                A6    
#define currentF2Pin	                A7    
#define modulation2Pin                  A8	    
#define modulation1Pin                  A9	    

// Constants
#define driverStartCurrent              25          // 51 per mA
#define finalStartCurrent               50          // 4.65 per mA
#define driverMinCurrent                240         // 51 per mA, In case of change, diagnostic must be adapted
#define driverRefCurrent                464         // 51 per mA, In case of change, diagnostic must be adapted
#define driverMaxCurrent                800         // 51 per mA, In case of change, diagnostic must be adapted
#define finalMaxCurrent                 688         // 4.65 per mA
#define minimalRefCurrent               186         // 4.65 per mA
#define maximalRefCurrent               558         // 4.65 per mA
#define dischargeMaxTime	        5   	    // seconds
#define heatMaxTime	                50   	    // seconds
#define highVoltageMaxTime	        12 	    // seconds
#define regulationMaxTime	        100   	    // seconds
#define functionOutOfRangeMaxTime	60          // seconds
#define buttonPressedStandByTime        3           // seconds 
#define buttonPressedMinTime            50          // milli-seconds
#define buttonPressedMaxTime            800         // milli-seconds
#define driverMasterP                   0.02
#define driverSlaveP                    0.015        
#define driverMasterI                   0.00003       
#define driverSlaveI                    0.0003
#define driverFunctionMasterI           0.000002
#define driverFunctionSlaveI            0.00002
#define finalMasterP                    0.007
#define finalSlaveP                     0.005        
#define finalMasterI                    0.00007       
#define finalSlaveI                     0.0007
#define finalFunctionMasterI            0.000004
#define finalFunctionSlaveI             0.00004
#define pidSampleTime                   100
#define driverRegulationTreshold        1            // 51 per mA
#define driverStabilizedTreshold        100          // 51 per mA
#define driverFunctionTreshold          25           // 51 per mA 
#define finalRegulationTreshold         1            // 4.65 per mA
#define finalStabilizedTreshold         60           // 4.65 per mA
#define finalFunctionTreshold           10           // 4.65 per mA 
#define driverAverageRatio              100 
#define finalAverageRatio               100 
#define slaveAverageRatio               3 
#define modulationPeakAverageRatio      1
#define modulationPeakReductionFactor   0.02
#define heatThinkTempMax                85           // > 85deg
#define airTempMax                      70           // > 70deg

// Indicator
#define indicatorMaxRange               180
#define indicatorCorrectionRatio        1.7
#define indicatorDetectStateMaxTime     800          // milli-seconds           
#define indicatorDisplayStateMaxTime    2000         // milli-seconds           
#define currentIndicatorRatio           0.226        // Max = 220
#define referenceIndicatorRatio         0.393        // Max current = maximalCurrent = 560 / Affichage max = 220
#define percentRatioIndicator	        2.0

// Internal use
Blink led;
SimpleTimer sendTimer;
SimpleTimer tempMeasureTimer;
SimpleTimer debugTimer;                // Debug
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
unsigned int finalMinCurrent;           //Initialized on reset() 
double currentD1Average = 0;
double currentD2Average = 0;
double currentD3Average = 0;
double currentD4Average = 0;
double currentF1Average = 0;
double currentF2Average = 0;
double currentF3Average = 0;
double currentF4Average = 0;
double modulation1PeakAverage = 0;
double modulation2PeakAverage = 0;
double modulationPeak;                   //Initialized on reset()  
double pidD1Output;                      //Initialized on resetRegulators()
double pidD2Output;                      //Initialized on resetRegulators() 
double pidD3Output;                      //Initialized on resetRegulators() 
double pidD4Output;                      //Initialized on resetRegulators() 
double pidF1Output;                      //Initialized on resetRegulators()
double pidF2Output;                      //Initialized on resetRegulators() 
double pidF3Output;                      //Initialized on resetRegulators() 
double pidF4Output;                      //Initialized on resetRegulators() 
unsigned int percentageSetPoint;         //Initialized on reset()
double pidDSetPoint;                     //Initialized on reset()
double pidD2SetPoint;                    //Initialized on reset()
double pidD4SetPoint;                    //Initialized on reset()
double pidFSetPoint;                     //Initialized on reset()
double pidF2SetPoint;                    //Initialized on reset()
double pidF4SetPoint;                    //Initialized on reset()
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int airTemp = 0;
unsigned int powerSupply1Temp = 0;
unsigned int powerSupply2Temp = 0;
unsigned int regulatorTemp = 0;
unsigned long dischargeStartTime;        //Initialized on reset()
unsigned long heatStartTime;             //Initialized on reset()
unsigned long highVoltageStartTime;      //Initialized on reset()
unsigned long regulationStartTime;       //Initialized on reset()
unsigned long functionStartTime;         //Initialized on reset()
unsigned long functionOutOfRangeTime;     //Initialized on reset()
unsigned long buttonPressedStartTime = 0;
unsigned int manualPercentageSetPoint = 20; 
unsigned int displayedPercentageSetPoint = 0;
boolean autoSetPoint = true;

// Debug
//char serialBuffer[12];
//String serialData = "";
//int delimiter = (int) '\n';

// Init regulators
PID pidD1(&currentD1Average, &pidD1Output, &pidDSetPoint, driverMasterP, driverMasterI, 0, 0, 255, pidSampleTime, false);
PID pidD2(&currentD2Average, &pidD2Output, &pidD2SetPoint, driverSlaveP, driverSlaveI, 0, 0, 255, pidSampleTime, false);
PID pidD3(&currentD3Average, &pidD3Output, &pidDSetPoint, driverMasterP, driverMasterI, 0, 0, 255, pidSampleTime, false);
PID pidD4(&currentD4Average, &pidD4Output, &pidD4SetPoint, driverSlaveP, driverSlaveI, 0, 0, 255, pidSampleTime, false);
PID pidF1(&currentF1Average, &pidF1Output, &pidFSetPoint, finalMasterP, finalMasterI, 0, 0, 255, pidSampleTime, false);
PID pidF2(&currentF2Average, &pidF2Output, &pidF2SetPoint, finalSlaveP, finalSlaveI, 0, 0, 255, pidSampleTime, false);
PID pidF3(&currentF3Average, &pidF3Output, &pidFSetPoint, finalMasterP, finalMasterI, 0, 0, 255, pidSampleTime, false);
PID pidF4(&currentF4Average, &pidF4Output, &pidF4SetPoint, finalSlaveP, finalSlaveI, 0, 0, 255, pidSampleTime, false);

// Timers definition
#define TIMER_CLK_STOP             0x00     // Timer Stopped
#define TIMER_CLK_DIV1             0x01     // Timer clocked at F_CPU
#define TIMER_CLK_DIV8             0x02     // Timer clocked at F_CPU/8
#define TIMER_CLK_DIV64            0x03     // Timer clocked at F_CPU/64
#define TIMER_CLK_DIV256           0x04     // Timer clocked at F_CPU/256
#define TIMER_CLK_DIV1024          0x05     // Timer clocked at F_CPU/1024
#define TIMER_PRESCALE_MASK        0x07

// Temp indexes in one wire bus
#define AIR_TEMPERATURE            0
#define POWERSUPPLY1_TEMPERATURE   1
#define REGULATOR_TEMPERATURE      2 
#define POWERSUPPLY2_TEMPERATURE   3 

// Sequence:
#define SEQ_DISCHARGE              0        // 0: Discharge
#define SEQ_HEAT                   1        // 1: Heat tempo 
#define SEQ_STARTING               2        // 2: Starting High Voltage
#define SEQ_REGULATING             3        // 3: Waiting for reg
#define SEQ_FUNCTION               4        // 4: Normal Fonction
#define SEQ_FAIL                   5        // 5: Fail
unsigned int  sequence = SEQ_DISCHARGE;

// Indicator states
#define IND_MIN                    0 
#define IND_NONE                   0        // Indicate nothing.
#define IND_CURRENT1               1        // Tube 1 current
#define IND_CURRENT2               2        // Tube 2 current
#define IND_CURRENT3               3        // Tube 3 current
#define IND_CURRENT4               4        // Tube 4 current
#define IND_SETWORKINGPOINT        5        // Working point swiping for manually set
#define IND_MAX                    5     
unsigned int  indicatorState = IND_NONE;
unsigned int  indicatorDetectState = IND_NONE;
unsigned long indicatorDisplayStateStartTime = 0;
unsigned long indicatorSetWorkingPointStartTime = 0;
unsigned long indicatorDetectStateStartTime = 0;

// Errors
#define NO_ERR                       0      // No error
#define ERR_DISHARGETOOLONG          2      // 2: Discharge too long
#define ERR_CURRENTONHEAT            3      // 3: Current during heat time
#define ERR_STARTINGOUTOFRANGE       4      // 4: Out of range during starting
#define ERR_REGULATINGTOOLONG        5      // 5: Stabilization too long
#define ERR_REGULATINGMAXREACHED     6      // 6: Maximun reached during regulation
#define ERR_REGULATINGMINREACHED     7      // 7: Minimum reached during regulation
#define ERR_FUNCTIONMAXREACHED       8      // 8: Maximun reached during normal function
#define ERR_FUNCTIONMINREACHED       9      // 9: Minimum reached during normal function
#define ERR_FUNCTIONOUTOFRANGE       10     // 10: Time elapsed with current out of range during normal function
#define ERR_STARTINGTOOLONG          11     // 11: Starting too long
#define ERR_TEMPTOOHIGH              12     // 12: Temperature maximum reached
byte errorNumber = NO_ERR;

#define ERR_TUBE_1       1
#define ERR_TUBE_2       2
#define ERR_TUBE_3       3
#define ERR_TUBE_4       4
#define ERR_TUBE_5       5
#define ERR_TUBE_6       6
#define ERR_TUBE_7       7
#define ERR_TUBE_8       8
#define ERR_TEMP_AIR     1
#define ERR_TEMP_PS1     2
#define ERR_TEMP_PS2     3
#define ERR_TEMP_REG     4
unsigned int errorCause = NO_ERR;
boolean displayTubeNumber = false;

#define CHECK_RANGE_OK       0 // Careful Do not change, this values are used as int 
#define CHECK_RANGE_TOOLOW   1
#define CHECK_RANGE_TOOHIGH  2

// Diagnostic
EasyTransfer dataTx; 
dataResponse dataTxStruct;

void reset(){
  dischargeStartTime = 0;
  heatStartTime = 0;
  highVoltageStartTime = 0;
  regulationStartTime = 0;
  functionOutOfRangeTime = 0;
  functionStartTime = 0;
  modulationPeak = 0;
  percentageSetPoint = 0;
  finalMinCurrent = 0;
  pidDSetPoint = driverRefCurrent;
  pidD2SetPoint = 0;
  pidD4SetPoint = 0;
  pidFSetPoint = minimalRefCurrent;
  pidF2SetPoint = 0;
  pidF4SetPoint = 0;
  resetRegulators();
}

void resetRegulators(){
  relayOff();

  pidD1.SetEnabled(false);  
  pidD2.SetEnabled(false);  
  pidD3.SetEnabled(false);  
  pidD4.SetEnabled(false);  

  pidF1.SetEnabled(false);  
  pidF2.SetEnabled(false);  
  pidF3.SetEnabled(false);  
  pidF4.SetEnabled(false);  

  pidD1Output = 0;
  pidD2Output = 0;
  pidD3Output = 0;
  pidD4Output = 0;

  pidF1Output = 0;
  pidF2Output = 0;
  pidF3Output = 0;
  pidF4Output = 0;

  analogWrite(regD1Pin, 0);
  analogWrite(regD2Pin, 0);
  analogWrite(regD3Pin, 0);
  analogWrite(regD4Pin, 0);

  analogWrite(regF1Pin, 0);
  analogWrite(regF2Pin, 0);
  analogWrite(regF3Pin, 0);
  analogWrite(regF4Pin, 0);
}

void initRegulators()
{
  if (sequence == SEQ_FUNCTION){
    pidD1.SetGains(0, driverFunctionMasterI, 0);
    pidD2.SetGains(0, driverFunctionSlaveI, 0);
    pidD3.SetGains(0, driverFunctionMasterI, 0);
    pidD4.SetGains(0, driverFunctionSlaveI, 0);

    pidF1.SetGains(0, finalFunctionMasterI, 0);
    pidF2.SetGains(0, finalFunctionSlaveI, 0);
    pidF3.SetGains(0, finalFunctionMasterI, 0);
    pidF4.SetGains(0, finalFunctionSlaveI, 0);
  }
  else{
    pidD1.SetGains(driverMasterP, driverMasterI, 0);
    pidD2.SetGains(driverSlaveP, driverSlaveI, 0);
    pidD3.SetGains(driverMasterP, driverMasterI, 0);
    pidD4.SetGains(driverSlaveP, driverSlaveI, 0);

    pidF1.SetGains(finalMasterP, finalMasterI, 0);
    pidF2.SetGains(finalSlaveP, finalSlaveI, 0);
    pidF3.SetGains(finalMasterP, finalMasterI, 0);
    pidF4.SetGains(finalSlaveP, finalSlaveI, 0);
  }

  pidD1.SetEnabled(true);
  pidD2.SetEnabled(true);
  pidD3.SetEnabled(true);
  pidD4.SetEnabled(true);
  pidF1.SetEnabled(true);
  pidF2.SetEnabled(true);
  pidF3.SetEnabled(true);
  pidF4.SetEnabled(true);
}

void computeDriverRegulators()
{  
  if (pidD1.Compute()) {
    analogWrite(regD1Pin, constrain((int)pidD1Output, 0, 255));
  }
  if (pidD2.Compute()) {
    analogWrite(regD2Pin, constrain((int)pidD2Output, 0, 255));
  }
  if (pidD3.Compute()) {
    analogWrite(regD3Pin, constrain((int)pidD3Output, 0, 255));
  }
  if (pidD4.Compute()) {
    analogWrite(regD4Pin, constrain((int)pidD4Output, 0, 255));
  }
}

void computeFinalRegulators()
{
  if (pidF1.Compute()) {
    analogWrite(regF1Pin, constrain((int)pidF1Output, 0, 255));
  }
  if (pidF2.Compute()) {
    analogWrite(regF2Pin, constrain((int)pidF2Output, 0, 255));
  }
  if (pidF3.Compute()) {
    analogWrite(regF3Pin, constrain((int)pidF3Output, 0, 255));
  }
  if (pidF4.Compute()) {
    analogWrite(regF4Pin, constrain((int)pidF4Output, 0, 255));
  }
}

void computeRegulators(){
  computeDriverRegulators();
  computeFinalRegulators();
}

unsigned int checkDriverInRange(double minValue, double maxValue)
{  
  if (minValue > 0){
    if (currentD1Average < minValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOLOW;
    } 
    if (currentD2Average < minValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOLOW;
    }
    if (currentD3Average < minValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOLOW;
    }
    if (currentD4Average < minValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0){
    if (currentD1Average > maxValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOHIGH;
    } 
    if (currentD2Average > maxValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOHIGH;
    }
    if (currentD3Average > maxValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOHIGH;
    }
    if (currentD4Average > maxValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERR;
  return CHECK_RANGE_OK;
}

unsigned int checkFinalInRange(double minValue, double maxValue)
{
  if (minValue > 0){
    if (currentF1Average < minValue)
    {
      errorCause = ERR_TUBE_5;
      return CHECK_RANGE_TOOLOW;
    } 
    if (currentF2Average < minValue)
    {
      errorCause = ERR_TUBE_6;
      return CHECK_RANGE_TOOLOW;
    }
    if (currentF3Average < minValue)
    {
      errorCause = ERR_TUBE_7;
      return CHECK_RANGE_TOOLOW;
    }
    if (currentF4Average < minValue)
    {
      errorCause = ERR_TUBE_8;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0){
    if (currentF1Average > maxValue)
    {
      errorCause = ERR_TUBE_5;
      return CHECK_RANGE_TOOHIGH;
    } 
    if (currentF2Average > maxValue)
    {
      errorCause = ERR_TUBE_6;
      return CHECK_RANGE_TOOHIGH;
    } 
    if (currentF3Average > maxValue)
    {
      errorCause = ERR_TUBE_7;
      return CHECK_RANGE_TOOHIGH;
    }
    if (currentF4Average > maxValue)
    {
      errorCause = ERR_TUBE_8;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERR;
  return CHECK_RANGE_OK;
}

unsigned int calcDriverRegulationProgress(double minValue, double maxValue, double range)
{
  double percentProgress = 100;

  if (currentD1Average < minValue)
  {
    percentProgress = 100 * (1 - (minValue - currentD1Average) / range);
  }
  else if (currentD1Average > maxValue)
  {
    percentProgress = 100 * (1 - (currentD1Average - maxValue) / range);
  }

  if (currentD2Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - currentD2Average) / range), percentProgress);
  }
  else if (currentD2Average > maxValue)
  {
    percentProgress = min(100 * (1 - (currentD2Average - maxValue) / range), percentProgress);
  }

  if (currentD3Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - currentD3Average) / range), percentProgress);
  }
  else if (currentD3Average > maxValue)
  {
    percentProgress = min(100 * (1 - (currentD3Average - maxValue) / range), percentProgress);
  }

  if (currentD4Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - currentD4Average) / range), percentProgress);
  }
  else if (currentD4Average > maxValue)
  {
    percentProgress = min(100 * (1 - (currentD4Average - maxValue) / range), percentProgress);
  }

  return constrain((int)percentProgress, 0, 100);
}

unsigned int calcFinalRegulationProgress(double minValue, double maxValue, double range)
{
  double percentProgress = 100;

  if (currentF1Average < minValue)
  {
    percentProgress = 100 * (1 - (minValue - currentF1Average) / range);
  }
  else if (currentF1Average > maxValue)
  {
    percentProgress = 100 * (1 - (currentF1Average - maxValue) / range);
  }

  if (currentF2Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - currentF2Average) / range), percentProgress);
  }
  else if (currentF2Average > maxValue)
  {
    percentProgress = min(100 * (1 - (currentF2Average - maxValue) / range), percentProgress);
  }

  if (currentF3Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - currentF3Average) / range), percentProgress);
  }
  else if (currentF3Average > maxValue)
  {
    percentProgress = min(100 * (1 - (currentF3Average - maxValue) / range), percentProgress);
  }

  if (currentF4Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - currentF4Average) / range), percentProgress);
  }
  else if (currentF4Average > maxValue)
  {
    percentProgress = min(100 * (1 - (currentF4Average - maxValue) / range), percentProgress);
  }

  return constrain((int)percentProgress, 0, 100);
}

void sendDatas()
{    
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(currentD1Average, 0, 1023, 0, 255);   // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(currentD2Average, 0, 1023, 0, 255);
  dataTxStruct.measure2 = map(currentD3Average, 0, 1023, 0, 255); 
  dataTxStruct.measure3 = map(currentD4Average, 0, 1023, 0, 255);
  dataTxStruct.measure4 = map(currentF1Average, 0, 1023, 0, 255);  
  dataTxStruct.measure5 = map(currentF2Average, 0, 1023, 0, 255);
  dataTxStruct.measure6 = map(currentF3Average, 0, 1023, 0, 255); 
  dataTxStruct.measure7 = map(currentF4Average, 0, 1023, 0, 255);
  dataTxStruct.output0 = constrain((int)pidD1Output, 0, 255);
  dataTxStruct.output1 = constrain((int)pidD2Output, 0, 255);
  dataTxStruct.output2 = constrain((int)pidD3Output, 0, 255);
  dataTxStruct.output3 = constrain((int)pidD4Output, 0, 255);
  dataTxStruct.output4 = constrain((int)pidF1Output, 0, 255);
  dataTxStruct.output5 = constrain((int)pidF2Output, 0, 255);
  dataTxStruct.output6 = constrain((int)pidF3Output, 0, 255);
  dataTxStruct.output7 = constrain((int)pidF4Output, 0, 255);
  dataTxStruct.temperature0 = constrain(airTemp, 0, 255);
  dataTxStruct.temperature1 = constrain(max(powerSupply1Temp, powerSupply2Temp), 0, 255); 
  dataTxStruct.temperature2 = constrain(regulatorTemp, 0, 255); 
  dataTxStruct.temperature3 = constrain(map(modulationPeak, 0, 1023, 0, 255), 0, 255);
  dataTxStruct.minValue = map(finalMinCurrent, 0, 1023, 0, 255);
  dataTxStruct.refValue = map(pidFSetPoint, 0, 1023, 0, 255); 
  dataTxStruct.maxValue = map(finalMaxCurrent, 0, 1023, 0, 255);  
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorCause;
  dataTx.sendData();

  if (modulationPeak > modulationPeakReductionFactor){
    modulationPeak -= modulationPeakReductionFactor;
  } 
  else {
    modulationPeak = 0;
  }
}

void measureTemperatures()
{
  // Send the command to get temperatures  
  tempSensors.requestTemperatures(); 
  airTemp = tempSensors.getTempCByIndex(AIR_TEMPERATURE);
  powerSupply1Temp = tempSensors.getTempCByIndex(POWERSUPPLY1_TEMPERATURE);
  powerSupply2Temp = tempSensors.getTempCByIndex(POWERSUPPLY2_TEMPERATURE);
  regulatorTemp = tempSensors.getTempCByIndex(REGULATOR_TEMPERATURE);
}

void relayOn(){
  analogWrite(relayPin, 127);
}

void relayOff(){
  analogWrite(relayPin, 0);
}

void regulate(){
  // Reset all elapsed time and force regulation
  functionStartTime = 0;
  regulationStartTime = 0;
  sequence = SEQ_REGULATING;
}

void calcMinCurrent(){
  finalMinCurrent = round(pidFSetPoint - 30);
}

void checkPushButton()
{
  // Only hard reset to leave from fail
  if (sequence == SEQ_FAIL) {
    return;
  } 

  unsigned long now = millis();
  if (digitalRead(buttonPin) == HIGH)
  {
    if (buttonPressedStartTime == 0){
      buttonPressedStartTime = now;
    }
  }
  else if (buttonPressedStartTime > 0)
  {    
    unsigned long milliSeconds = now - buttonPressedStartTime; 
    if (milliSeconds >= buttonPressedMinTime && milliSeconds <= buttonPressedMaxTime){
      // If working point is in selection, set the working point manually with the current displayed value.
      if (indicatorState == IND_SETWORKINGPOINT) {
        manualPercentageSetPoint = displayedPercentageSetPoint;
        autoSetPoint = false;
        indicatorState = IND_CURRENT1;
      } 
      else {
        // Increment indicator state 
        indicatorDetectState++;
        if (indicatorDetectState > IND_MAX){
          indicatorDetectState = IND_NONE;
        }
      }
      indicatorDetectStateStartTime = now;
    }
    else {
      // Cancel the operation
      indicatorDetectStateStartTime = 0;
      indicatorDetectState = IND_NONE;      
    }

    buttonPressedStartTime = 0;
  }
  else if (indicatorDetectStateStartTime > 0 && now - indicatorDetectStateStartTime > indicatorDetectStateMaxTime){
    setIndicatorState(indicatorDetectState);
    indicatorDetectState = IND_NONE;
    indicatorDetectStateStartTime = 0;
  }
}

void setIndicatorState(byte state){
  if (state == IND_SETWORKINGPOINT && !autoSetPoint) {
    autoSetPoint = true;
    indicatorState = IND_CURRENT1;
  }
  else{
    indicatorDisplayStateStartTime = millis();
    indicatorState = state;
  }
}

void displayIndicator()
{
  if (indicatorState == IND_SETWORKINGPOINT) {
    // Set working point manually, move indicator from end to start to let the user click on the desired current
    unsigned long now = millis();
    if (indicatorSetWorkingPointStartTime == 0) {
      indicatorSetWorkingPointStartTime = now;
    }

    // Decrease of 10% per seconds
    unsigned long elapsed = now - indicatorSetWorkingPointStartTime;
    if (elapsed < 500){
      displayedPercentageSetPoint = 100;
    }
    else if (elapsed > 11000){
      // Too late 
      indicatorState = IND_NONE;            
    }
    else if (elapsed > 10500){
      displayedPercentageSetPoint = 0;
    }
    else{
      displayedPercentageSetPoint = 100 - (elapsed - 500) / 100;
    }

    analogWrite(indicatorPin, displayedPercentageSetPoint * percentRatioIndicator);
  } 
  else {
    indicatorSetWorkingPointStartTime = 0;

    if (millis() - indicatorDisplayStateStartTime < indicatorDisplayStateMaxTime)
    {
      analogWrite(indicatorPin, indicatorState * percentRatioIndicator * 25);      
    }
    else
    {
      switch (indicatorState)
      {
      case IND_CURRENT1:
        // Vu-meter read working left position 1
        analogWrite(indicatorPin, currentF1Average * currentIndicatorRatio);     
        break;   

      case IND_CURRENT2:
        // Vu-meter read working left position 2
        analogWrite(indicatorPin, currentF2Average * currentIndicatorRatio);     
        break;     

      case IND_CURRENT3:
        // Vu-meter read working right position 1
        analogWrite(indicatorPin, currentF3Average * currentIndicatorRatio);     
        break;   

      case IND_CURRENT4:
        // Vu-meter read working right position 2
        analogWrite(indicatorPin, currentF4Average * currentIndicatorRatio);     
        break;     

      default:     
        // IND_NONE:
        analogWrite(indicatorPin, 0);
      }
    }
  } 
}

unsigned int calcPercentSetPoint(){
  if (modulationPeak > 900) {
    return 100;
  } 
  if (modulationPeak > 620) {
    return 75;
  } 
  if (modulationPeak > 350) {
    return 50;
  } 
  if (modulationPeak > 100) {
    return 25;
  } 
  return 0;
}

void debugFn(){
  // Debug
  /*Serial.print("modulation: ");
   Serial.println(analogRead(modulationPin));
   Serial.print("currentMax: ");
   Serial.println(currentMax);
   Serial.print("modulationPeakAverage: ");
   Serial.println(modulationPeakAverage);
   Serial.println();*/

  /*while (Serial.available()) {
   int ch = Serial.read();
   if (ch == -1) {
   }
   else if (ch == delimiter) {
   break;
   }
   else {
   serialData += (char) ch;
   }
   }
   
   int serialLength = serialData.length() + 1;
   if (serialLength > 1){
   serialData.toCharArray(serialBuffer, serialLength);
   
   serialData = "";
   
   unsigned int percentage = atoi(serialBuffer);
   
   pidSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent; 
   regulate();
   
   Serial.print("Set point set to: ");    
   Serial.println(pidSetPoint);
   }*/
}

// the setup routine runs once when you press reset:
void setup() {                
  Serial.begin(9600);

  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);     
  pinMode(buttonPin, INPUT);
  pinMode(oneWireBusPin, OUTPUT);    

  // Set pwm speed to the maximum (32KHZ)
  TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR3B = (TCCR3B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR4B = (TCCR4B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

  reset();

  digitalWrite(oneWireBusPin, HIGH);

  led.Setup(ledPin, false);
  sendTimer.setInterval(200, sendDatas);
  debugTimer.setInterval(1000, debugFn);

  tempSensors.begin();   
  tempMeasureTimer.setInterval(30000, measureTemperatures);
  tempSensors.requestTemperatures(); 
  measureTemperatures();

  // Diagnostic
  Serial1.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial1);

  analogReference(INTERNAL1V1);
}

// the loop routine runs over and over again forever:
void loop() 
{ 
  unsigned int currentTime;
  unsigned int check;

  if (sequence != SEQ_FAIL){
    if (airTemp > airTempMax) {
      // Fail, air temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_AIR;
    }
    else if (powerSupply1Temp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PS1;
    }
    else if (powerSupply2Temp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PS2;
    }
    else if (regulatorTemp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_REG;
    }
    else {
      // Read and smooth the input
      currentD1Average += (analogRead(currentD1Pin)-currentD1Average)/driverAverageRatio;
      currentD2Average += (analogRead(currentD2Pin)-currentD2Average)/driverAverageRatio;
      currentD3Average += (analogRead(currentD3Pin)-currentD3Average)/driverAverageRatio;
      currentD4Average += (analogRead(currentD4Pin)-currentD4Average)/driverAverageRatio;    

      unsigned int current1 = analogRead(currentF1Pin);
      unsigned int current2 = analogRead(currentF2Pin);
      unsigned int current3 = analogRead(currentF3Pin);
      unsigned int current4 = analogRead(currentF4Pin);
      currentF1Average += (current1-currentF1Average)/finalAverageRatio;
      currentF2Average += (current2-currentF2Average)/finalAverageRatio;
      currentF3Average += (current3-currentF3Average)/finalAverageRatio;
      currentF4Average += (current4-currentF4Average)/finalAverageRatio;    

      if (sequence >= SEQ_REGULATING) {
        modulation1PeakAverage += (analogRead(modulation1Pin)-(0.132*max(current1, current2))-modulation1PeakAverage)/modulationPeakAverageRatio;
        modulation2PeakAverage += (analogRead(modulation2Pin)-(0.132*max(current3, current4))-modulation2PeakAverage)/modulationPeakAverageRatio;

        // Calcul du facteur de la mesure de modulation du aux diviseur d'entrée
        //33K/(3.3K+51K//51K)=0.116  

        // Facteur d'augmentation dans l'enroulement de cathode du transfo
        //=1.33

        if (modulationPeak < modulation1PeakAverage)
        {
          modulationPeak = modulation1PeakAverage;
        }

        if (modulationPeak < modulation2PeakAverage)
        {
          modulationPeak = modulation2PeakAverage;
        }
      }

      // Calc regulators set point
      unsigned int percentage = autoSetPoint ? calcPercentSetPoint() : manualPercentageSetPoint;
      if (percentageSetPoint != percentage){
        // New set point must be set
        pidFSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent;     
        if (sequence == SEQ_FUNCTION) {
          regulate();
        }   
        percentageSetPoint = percentage;
      }

      // Average set points for slave regulators from master measure
      pidD2SetPoint += (currentD1Average-pidD2SetPoint)/slaveAverageRatio; 
      pidD4SetPoint += (currentD3Average-pidD4SetPoint)/slaveAverageRatio; 
      pidF2SetPoint += (currentF1Average-pidF2SetPoint)/slaveAverageRatio; 
      pidF4SetPoint += (currentF3Average-pidF4SetPoint)/slaveAverageRatio; 

      checkPushButton();   
      displayIndicator();
    }
  }

  // Diagnostic
  sendTimer.run();

  // Debug
  //debugTimer.run();

  switch (sequence)
  {  
  case SEQ_DISCHARGE:	
    // Discharging

    // Reset errors
    errorCause = NO_ERR;
    errorNumber = NO_ERR;

    // Pre-sequence
    if (dischargeStartTime == 0){
      dischargeStartTime = millis();
      resetRegulators();
    }

    led.Execute(800, 200);

    // Calc elapsed time in seconds
    currentTime = (millis() - dischargeStartTime) / 1000;

    // Diagnostic
    stepMaxTime = dischargeMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = 0;
    stepCurValue = 1;

    if(currentTime > dischargeMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_DISHARGETOOLONG;
      break;
    }

    if(checkDriverInRange(0, driverStartCurrent) == CHECK_RANGE_TOOHIGH)
    {
      break;
    }

    if(checkFinalInRange(0, finalStartCurrent) == CHECK_RANGE_TOOHIGH)
    {
      break;
    }

    // Post-sequence
    dischargeStartTime = 0;
    sequence++;

  case SEQ_HEAT: 
    // Startup tempo 

    // Pre-sequence
    if (heatStartTime == 0){
      heatStartTime = millis();
      resetRegulators();
    }

    led.Execute(400, 400);

    // Calc elapsed time in seconds
    currentTime = (millis() - heatStartTime) / 1000;

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = currentTime;

    // Ensure no current at this step
    if(checkDriverInRange(0, driverStartCurrent + 10) == CHECK_RANGE_TOOHIGH)
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }

    if(checkFinalInRange(0, finalStartCurrent + 10) == CHECK_RANGE_TOOHIGH)
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }

    if(currentTime < heatMaxTime)
    {
      break;
    }

    // Diagnostic, force 100%
    stepElapsedTime = heatMaxTime;
    stepCurValue = heatMaxTime;

    // Post-sequence
    heatStartTime = 0;
    led.On();
    measureTemperatures();
    sequence++;
    delay(500);   

  case SEQ_STARTING:
    // Starting High Voltage

    // Pre-sequence
    if (highVoltageStartTime == 0){
      highVoltageStartTime = millis();
      relayOn();
      initRegulators();
    }

    // Regulation
    computeRegulators(); 

    led.Execute(20, 400);  

    // Calc elapsed time in seconds
    currentTime = (millis() - highVoltageStartTime) / 1000;

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = 200;
    stepCurValue = calcDriverRegulationProgress(driverRefCurrent/2, driverRefCurrent + driverStabilizedTreshold, driverRefCurrent - driverStabilizedTreshold); 
    stepCurValue += calcFinalRegulationProgress(pidFSetPoint/2, pidFSetPoint + finalStabilizedTreshold, pidFSetPoint - finalStabilizedTreshold); 

    if(currentTime > highVoltageMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGTOOLONG;
      break;
    }

    if (checkDriverInRange(0, driverMaxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGOUTOFRANGE;
      break;      
    }

    if (checkFinalInRange(0, finalMaxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGOUTOFRANGE;
      break;      
    }

    // If target points not reached, continue to regulate
    check = checkDriverInRange(driverRefCurrent/2, driverRefCurrent + driverStabilizedTreshold) + checkFinalInRange(pidFSetPoint/2, pidFSetPoint + finalStabilizedTreshold);
    if(check != CHECK_RANGE_OK)
    {
      break;  
    }  

    // Post-sequence
    highVoltageStartTime = 0;
    sequence++;

  case SEQ_REGULATING: 
    // Waiting for reg       

    // Pre-sequence
    if (regulationStartTime == 0){
      regulationStartTime = millis();     
      finalMinCurrent = 0; 
      relayOn();
      initRegulators();
    }

    // Regulation
    computeRegulators(); 

    led.Execute(20, 1500);    

    // Calc elapsed time in seconds
    currentTime = (millis() - regulationStartTime) / 1000;

    // Diagnostic
    stepMaxTime = regulationMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = 200;
    stepCurValue = calcDriverRegulationProgress(driverRefCurrent - driverRegulationTreshold, driverRefCurrent + driverRegulationTreshold, driverStabilizedTreshold - driverRegulationTreshold); 
    stepCurValue += calcFinalRegulationProgress(pidFSetPoint - finalRegulationTreshold, pidFSetPoint + finalRegulationTreshold, finalStabilizedTreshold - finalRegulationTreshold); 

    if(currentTime > regulationMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGTOOLONG;
      break;
    }

    if (checkDriverInRange(driverRefCurrent/2, driverMaxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGMAXREACHED;
      break;      
    }

    if (checkFinalInRange(0, finalMaxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGMAXREACHED;
      break;      
    }

    // If target points not reached, continue to regulate
    if(checkDriverInRange(driverRefCurrent - driverRegulationTreshold, driverRefCurrent + driverRegulationTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    if (checkFinalInRange(pidFSetPoint - finalRegulationTreshold, pidFSetPoint + finalRegulationTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    calcMinCurrent();
    regulationStartTime = 0;
    sequence++;

  case SEQ_FUNCTION:
    // Normal Fonction    

    // Pre-sequence
    if (functionStartTime == 0){
      functionStartTime = millis();
      relayOn();
      initRegulators();
      led.Off();
    }

    // Regulation
    computeRegulators(); 

    // Calc elapsed time in minuts
    currentTime = (millis() - functionStartTime) / 60000; 

    // Diagnostic
    tempMeasureTimer.run();
    stepMaxTime = 0;
    stepElapsedTime = currentTime;
    stepMaxValue = 0;
    stepCurValue = 0;

    check = checkDriverInRange(driverMinCurrent, driverMaxCurrent);
    if(check != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
      break;
    }

    check = checkFinalInRange(finalMinCurrent, finalMaxCurrent);
    if(check != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
      break;
    }

    check = checkDriverInRange(driverRefCurrent - driverFunctionTreshold, driverRefCurrent + driverFunctionTreshold) + checkFinalInRange(pidFSetPoint - finalFunctionTreshold, pidFSetPoint + finalFunctionTreshold);
    if(check != CHECK_RANGE_OK)
    {
      if (functionOutOfRangeTime == 0){
        functionOutOfRangeTime = millis();
      }

      if (millis() - functionOutOfRangeTime > functionOutOfRangeMaxTime * 1000)
      {    
        // Fail out of range error
        sequence = SEQ_FAIL;
        errorNumber = ERR_FUNCTIONOUTOFRANGE;
        break;
      }          
    }
    else{
      functionOutOfRangeTime = 0;
    }
    break;

  default: 
    // Fail, protect mode
    relayOff();

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;

    led.Execute(250, displayTubeNumber ? errorCause : errorNumber, 1200);
  } 
}










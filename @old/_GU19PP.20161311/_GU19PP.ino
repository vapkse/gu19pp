/*
  GU19 PP
  Version 2.0.0
  Date: 14.09.2015
  Loaded with USB

  15.04.2015 - 1.2.0: Added current source in the driver
  20.04.2015 - 1.2.1: Added temporisation on currents overshoot
  14.09.2015 - 1.3.0: Added average on temperature measurement
  14.03.2016 - 1.3.1: Changed minimum current limit, make better button detection
  18.09.2016 - 2.0.0: Differential feedback
*/

#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <EEPROM.h>
#include <DallasTemperature.h>

static const byte PPGU19_ID = 19;
static const byte ampId = PPGU19_ID;

// Constants
#define PARAMS_MEMORY_ADDRESS 0
#define PID_SAMPLE_TIME 100
#define REG_COUNT 4
#define MOD_LIMIT_COUNT 4
#define TEMP_COUNT 4

// Pin Config
#define regD4Pin 2
#define regD3Pin 3
#define oneWireBusPin 4
#define regF3Pin 5
#define regF4Pin 6
#define regD2Pin 7
#define regD1Pin 8
#define regF2Pin 9
#define regF1Pin 10
#define indicatorPin 11
#define relayPin 12
#define ledPin 13
#define leftDFeedbackPin 32
#define rightDFeedbackPin 33
#define buttonPin 35
#define currentF4Pin A2
#define currentF3Pin A3
#define currentF1Pin A6
#define currentF2Pin A7
#define modulation2Pin A8
#define modulation1Pin A9

// Constants
#define startCurrent 50                            // 4.65 per mA
#define maxCurrent 688                             // 4.65 per mA
#define minimalRefCurrent 186                      // 4.65 per mA
#define maximalRefCurrent 558                      // 4.65 per mA
#define dischargeMinTime 1                         // Seconds
#define dischargeMaxTime 5                         // Seconds
#define heatMaxTime 50                             // Seconds
#define highVoltageMaxTime 15                      // Seconds
#define regulationMaxTime 150                      // Seconds
#define outOfRangeMaxTime 300                      // Seconds
#define buttonPressedMinTime 50                    // Milli-seconds
#define buttonNextFunctionMaxTime 650              // Milli-seconds
#define buttonSetWorkingPointFunctionMaxTime 12000 // Milli-seconds
#define buttonSoftResetMinPressTime 22000          // Milli-seconds
#define errorMaxTime 500                           // Milli-seconds
#define masterP 0.005
#define slaveP 0.003
#define masterI 0.00003
#define slaveI 0.0003
#define functionMasterI 0.000004
#define functionSlaveI 0.00004
#define pidSampleTime 100
#define regulationTreshold 1  // 4.65 per mA
#define stabilizedTreshold 60 // 4.65 per mA
#define functionTreshold 20   // 4.65 per mA
#define averageRatio 100
#define slaveAverageRatio 3
#define modulationPeakAverageRatio 1
#define modulationPeakReductionFactor 0.02
#define modulationDetectedMinTime 5000 // Milli-seconds
#define heatThinkTempMax 85            // > 85deg
#define airTempMax 70                  // > 70deg
#define temperatureAverageRatio 5
#define diagnosticSendMinTime 200 // Milli-seconds
#define tempMeasureMinTime 3000   // Milli-seconds
#define driverFixedSetPoint 50
#define writeEepromDelay 30000

// Indicator
#define currentIndicatorRatio 0.226 // Max = 2200
#define percentRatioIndicator 2.0

// Internal use
Blink led;
unsigned long lastDiagnosticTime = 0;
unsigned long lastTempMeasureTime = 0;
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
unsigned int minCurrent; // Initialized on reset()
double currentF1Average = 0;
double currentF2Average = 0;
double currentF3Average = 0;
double currentF4Average = 0;
double modulation1PeakAverage = 0;
double modulation2PeakAverage = 0;
double modulationPeak; // Initialized on reset()
unsigned long modulationDetectedTime = 0;
double pid1Output; // Initialized on resetRegulators()
double pid2Output; // Initialized on resetRegulators()
double pid3Output; // Initialized on resetRegulators()
double pid4Output; // Initialized on resetRegulators()
unsigned int percentageSetPoint; // Initialized on reset()
double pidSetPoint; // Initialized on reset()
double pid2SetPoint; // Initialized on reset()
double pid4SetPoint; // Initialized on reset()
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int airTemp = 0;
unsigned int powerSupply1Temp = 0;
unsigned int powerSupply2Temp = 0;
unsigned long sequenceStartTime; // Initialized on reset()
unsigned long outOfRangeTime; // Initialized on reset()
unsigned long errorTime; // Initialized on reset()
unsigned int regulatorTemp = 0;
unsigned int manualPercentageSetPoint = 20;
unsigned int displayedPercentageSetPoint = 0;
boolean autoSetPoint = true;
boolean dfeedState = false;
unsigned long buttonLastPressedTime = 0;
boolean buttonState = false;
unsigned int buttonFunction = 0;

// Init regulators
PID pid1(&currentF1Average, &pid1Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, false);
PID pid2(&currentF2Average, &pid2Output, &pid2SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, false);
PID pid3(&currentF3Average, &pid3Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, false);
PID pid4(&currentF4Average, &pid4Output, &pid4SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, false);

// Timers definition
#define TIMER_CLK_DIV1 0x01 // Timer clocked at F_CPU
#define TIMER_PRESCALE_MASK 0x07

// Temp indexes in one wire bus
#define AIR_TEMPERATURE 0
#define POWERSUPPLY1_TEMPERATURE 1
#define REGULATOR_TEMPERATURE 2
#define POWERSUPPLY2_TEMPERATURE 3

// Sequence:
#define SEQ_DISCHARGE 0  // 0: Discharge
#define SEQ_HEAT 1       // 1: Heat tempo
#define SEQ_STARTING 2   // 2: Starting High Voltage
#define SEQ_REGULATING 3 // 3: Waiting for reg
#define SEQ_FUNCTION 4   // 4: Normal Function
#define SEQ_FAIL 5       // 5: Fail
byte sequence = SEQ_DISCHARGE;

// Indicator states
#define BUTTONSTATE_NONE 0               // Indicate nothing.
#define BUTTONSTATE_CURRENT1 1           // Tube 1 current
#define BUTTONSTATE_CURRENT2 2           // Tube 2 current
#define BUTTONSTATE_CURRENT3 3           // Tube 3 current
#define BUTTONSTATE_CURRENT4 4           // Tube 4 current
#define BUTTONSTATE_DIFFFEED 5           // Switch differential feedback
#define BUTTONSTATE_SELECTWORKINGPOINT 6 // Working point swaping for manually set
#define BUTTONSTATE_AUTOWORKINGPOINT 7
#define BUTTONSTATE_SOFTRESET 8
unsigned long indicatorSetWorkingPointStartTime = 0;

// Errors
#define NO_ERR 0                   // No error
#define ERR_DISHARGETOOLONG 2      // 2: Discharge too long
#define ERR_CURRENTONHEAT 3        // 3: Current during heat time
#define ERR_STARTINGOUTOFRANGE 4   // 4: Out of range during starting
#define ERR_REGULATINGTOOLONG 5    // 5: Regulation too long
#define ERR_REGULATINGMAXREACHED 6 // 6: Maximun reached during regulation
#define ERR_REGULATINGMINREACHED 7 // 7: Minimum reached during regulation
#define ERR_FUNCTIONMAXREACHED 8   // 8: Maximun reached during normal function
#define ERR_FUNCTIONMINREACHED 9   // 9: Minimum reached during normal function
#define ERR_FUNCTIONOUTOFRANGE 10  // 10: Time elapsed with current out of range during normal function
#define ERR_STARTINGTOOLONG 11     // 11: Starting too long
#define ERR_TEMPTOOHIGH 12         // 12: Temperature maximum reached
byte errorNumber = NO_ERR;

#define ERR_TUBE_1 1
#define ERR_TUBE_2 2
#define ERR_TUBE_3 3
#define ERR_TUBE_4 4
#define ERR_TUBE_5 5
#define ERR_TUBE_6 6
#define ERR_TUBE_7 7
#define ERR_TUBE_8 8
#define ERR_TEMP_AIR 1
#define ERR_TEMP_PS1 2
#define ERR_TEMP_PS2 3
#define ERR_TEMP_REG 4
byte errorCause = NO_ERR;
boolean displayTubeNumber = false;

#define CHECK_RANGE_OK 0 // Careful Do not change, this values are used as int
#define CHECK_RANGE_TOOLOW 1
#define CHECK_RANGE_TOOHIGH 2

// Diagnostic
// Diagnostic, buffers allocation
SerialBuffer params;
paramsResponse *paramsBuffer = (paramsResponse *)params.alloc(sizeof(struct paramsResponse));

SerialBuffer response;
SerialResponse *responseBuffer = (SerialResponse *)response.alloc(sizeof(struct DataResponse));

SerialBuffer request;
SerialRequest *requestBuffer = (SerialRequest *)request.alloc(sizeof(struct SerialRequest));

// Eeprom
unsigned long writeEepromTime = 0;

void reset() {
    sequenceStartTime = 0;
    outOfRangeTime = 0;
    errorTime = 0;
    modulationPeak = 0;
    percentageSetPoint = 0;
    minCurrent = 0;
    pidSetPoint = minimalRefCurrent;
    pid2SetPoint = 0;
    pid4SetPoint = 0;
    resetRegulators();
}

void resetRegulators() {
    relayOff();

    pid1.SetEnabled(false);
    pid2.SetEnabled(false);
    pid3.SetEnabled(false);
    pid4.SetEnabled(false);

    pid1Output = 0;
    pid2Output = 0;
    pid3Output = 0;
    pid4Output = 0;

    analogWrite(regD1Pin, 0);
    analogWrite(regD2Pin, 0);
    analogWrite(regD3Pin, 0);
    analogWrite(regD4Pin, 0);

    analogWrite(regF1Pin, 0);
    analogWrite(regF2Pin, 0);
    analogWrite(regF3Pin, 0);
    analogWrite(regF4Pin, 0);
}

void initRegulators() {
    if (sequence == SEQ_FUNCTION) {
        pid1.SetGains(0, functionMasterI, 0);
        pid2.SetGains(0, functionSlaveI, 0);
        pid3.SetGains(0, functionMasterI, 0);
        pid4.SetGains(0, functionSlaveI, 0);
    } else {
        pid1.SetGains(masterP, masterI, 0);
        pid2.SetGains(slaveP, slaveI, 0);
        pid3.SetGains(masterP, masterI, 0);
        pid4.SetGains(slaveP, slaveI, 0);
    }

    analogWrite(regD1Pin, driverFixedSetPoint);
    analogWrite(regD2Pin, driverFixedSetPoint);
    analogWrite(regD3Pin, driverFixedSetPoint);
    analogWrite(regD4Pin, driverFixedSetPoint);

    pid1.SetEnabled(true);
    pid2.SetEnabled(true);
    pid3.SetEnabled(true);
    pid4.SetEnabled(true);
}

void computeRegulators() {
    if (pid1.Compute()) {
        analogWrite(regF1Pin, constrain((int)pid1Output, 0, 255));
    }
    if (pid2.Compute()) {
        analogWrite(regF2Pin, constrain((int)pid2Output, 0, 255));
    }
    if (pid3.Compute()) {
        analogWrite(regF3Pin, constrain((int)pid3Output, 0, 255));
    }
    if (pid4.Compute()) {
        analogWrite(regF4Pin, constrain((int)pid4Output, 0, 255));
    }
}

unsigned int checkInRange(double minValue, double maxValue) {
    if (minValue > 0) {
        if (currentF1Average < minValue) {
            errorCause = ERR_TUBE_5;
            return CHECK_RANGE_TOOLOW;
        }
        if (currentF2Average < minValue) {
            errorCause = ERR_TUBE_6;
            return CHECK_RANGE_TOOLOW;
        }
        if (currentF3Average < minValue) {
            errorCause = ERR_TUBE_7;
            return CHECK_RANGE_TOOLOW;
        }
        if (currentF4Average < minValue) {
            errorCause = ERR_TUBE_8;
            return CHECK_RANGE_TOOLOW;
        }
    }

    if (maxValue > 0) {
        if (currentF1Average > maxValue) {
            errorCause = ERR_TUBE_5;
            return CHECK_RANGE_TOOHIGH;
        }
        if (currentF2Average > maxValue) {
            errorCause = ERR_TUBE_6;
            return CHECK_RANGE_TOOHIGH;
        }
        if (currentF3Average > maxValue) {
            errorCause = ERR_TUBE_7;
            return CHECK_RANGE_TOOHIGH;
        }
        if (currentF4Average > maxValue) {
            errorCause = ERR_TUBE_8;
            return CHECK_RANGE_TOOHIGH;
        }
    }

    errorCause = NO_ERR;
    return CHECK_RANGE_OK;
}

double calcRegulationProgress(double minValue, double maxValue, double range) {
    double percentProgress = 100;

    if (currentF1Average < minValue) {
        percentProgress = 100 * (1 - (minValue - currentF1Average) / range);
    } else if (currentF1Average > maxValue) {
        percentProgress = 100 * (1 - (currentF1Average - maxValue) / range);
    }

    if (currentF2Average < minValue) {
        percentProgress = min(100 * (1 - (minValue - currentF2Average) / range), percentProgress);
    } else if (currentF2Average > maxValue) {
        percentProgress = min(100 * (1 - (currentF2Average - maxValue) / range), percentProgress);
    }

    if (currentF3Average < minValue) {
        percentProgress = min(100 * (1 - (minValue - currentF3Average) / range), percentProgress);
    } else if (currentF3Average > maxValue) {
        percentProgress = min(100 * (1 - (currentF3Average - maxValue) / range), percentProgress);
    }

    if (currentF4Average < minValue) {
        percentProgress = min(100 * (1 - (minValue - currentF4Average) / range), percentProgress);
    } else if (currentF4Average > maxValue) {
        percentProgress = min(100 * (1 - (currentF4Average - maxValue) / range), percentProgress);
    }

    return constrain((int)percentProgress, 0, 100);
}

void sendDatas() {
    // Send datas
    dataTxStruct.message = MESSAGE_SENDVALUES;
    dataTxStruct.step = sequence;
    dataTxStruct.stepMaxTime = stepMaxTime;
    dataTxStruct.stepElapsedTime = stepElapsedTime;
    dataTxStruct.stepMaxValue = stepMaxValue;
    dataTxStruct.stepCurValue = stepCurValue;
    dataTxStruct.tickCount = millis();
    dataTxStruct.measure0 = map(currentF1Average, 0, 1023, 0, 255);
    dataTxStruct.measure1 = map(currentF2Average, 0, 1023, 0, 255);
    dataTxStruct.measure2 = map(currentF3Average, 0, 1023, 0, 255);
    dataTxStruct.measure3 = map(currentF4Average, 0, 1023, 0, 255);
    dataTxStruct.output0 = constrain((int)pid1Output, 0, 255);
    dataTxStruct.output1 = constrain((int)pid2Output, 0, 255);
    dataTxStruct.output2 = constrain((int)pid3Output, 0, 255);
    dataTxStruct.output3 = constrain((int)pid4Output, 0, 255);
    dataTxStruct.temperature0 = constrain(airTemp, 0, 255);
    dataTxStruct.temperature1 = constrain(max(powerSupply1Temp, powerSupply2Temp), 0, 255);
    dataTxStruct.temperature2 = constrain(regulatorTemp, 0, 255);
    dataTxStruct.temperature3 = constrain(map(modulationPeak, 0, 1023, 0, 255), 0, 255);
    dataTxStruct.minValue = map(minCurrent, 0, 1023, 0, 255);
    dataTxStruct.refValue = map(pidSetPoint, 0, 1023, 0, 255);
    dataTxStruct.maxValue = map(maxCurrent, 0, 1023, 0, 255);
    dataTxStruct.errorNumber = errorNumber;
    dataTxStruct.errorTube = errorCause;
    dataTx.sendData();

    if (modulationPeak > modulationPeakReductionFactor) {
        modulationPeak -= modulationPeakReductionFactor;
    } else {
        modulationPeak = 0;
    }

    lastDiagnosticTime = millis();
}

void measureTemperatures(int tempAverageRatio) {
    // Send the command to get temperatures
    tempSensors.requestTemperatures();
    airTemp += (tempSensors.getTempCByIndex(AIR_TEMPERATURE) - airTemp) / tempAverageRatio;
    powerSupply1Temp += (tempSensors.getTempCByIndex(POWERSUPPLY1_TEMPERATURE) - powerSupply1Temp) / tempAverageRatio;
    powerSupply2Temp += (tempSensors.getTempCByIndex(POWERSUPPLY2_TEMPERATURE) - powerSupply2Temp) / tempAverageRatio;
    regulatorTemp += (tempSensors.getTempCByIndex(REGULATOR_TEMPERATURE) - regulatorTemp) / tempAverageRatio;
    lastTempMeasureTime = millis();
}

void relayOn() { analogWrite(relayPin, 127); }

void relayOff() { analogWrite(relayPin, 0); }

void setDiffFeed() {
    digitalWrite(leftDFeedbackPin, dfeedState ? LOW : HIGH);
    digitalWrite(rightDFeedbackPin, dfeedState ? LOW : HIGH);
}

void regulate() {
    // Reset all elapsed time and force regulation
    sequenceStartTime = 0;
    sequence = SEQ_REGULATING;
}

void calcMinCurrent() { minCurrent = round(pidSetPoint * 0.5); }

void displayIndicator() {
    unsigned long currentTime = millis();

    buttonState = digitalRead(buttonPin);
    if (buttonState) {
        if (buttonLastPressedTime == 0) {
            buttonLastPressedTime = currentTime;
        }
    } else {
        buttonLastPressedTime = 0;
    }

    if (buttonLastPressedTime > 0) {
        if (currentTime - buttonLastPressedTime > buttonSoftResetMinPressTime) {
            buttonFunction = BUTTONSTATE_SOFTRESET;
        } else if (currentTime - buttonLastPressedTime > 5500 + buttonSetWorkingPointFunctionMaxTime) {
            buttonFunction = BUTTONSTATE_AUTOWORKINGPOINT;
        } else if (currentTime - buttonLastPressedTime > 5500) {
            buttonFunction = BUTTONSTATE_SELECTWORKINGPOINT;
        } else if (currentTime - buttonLastPressedTime > 4500) {
            buttonFunction = BUTTONSTATE_CURRENT4;
        } else if (currentTime - buttonLastPressedTime > 3500) {
            buttonFunction = BUTTONSTATE_CURRENT3;
        } else if (currentTime - buttonLastPressedTime > 2500) {
            buttonFunction = BUTTONSTATE_CURRENT2;
        } else if (currentTime - buttonLastPressedTime > 1500) {
            buttonFunction = BUTTONSTATE_CURRENT1;
        } else if (currentTime - buttonLastPressedTime > 50) {
            buttonFunction = BUTTONSTATE_DIFFFEED;
        } else {
            buttonFunction = BUTTONSTATE_NONE;
        }
    }

    if (buttonFunction > BUTTONSTATE_NONE) {
        if (buttonLastPressedTime > 0) {
            // Display function
            if (buttonFunction <= BUTTONSTATE_CURRENT4) {
                analogWrite(indicatorPin, constrain((int)(buttonFunction * percentRatioIndicator * 25), 0, 255));
            } else if (buttonFunction == BUTTONSTATE_DIFFFEED) {
                analogWrite(indicatorPin, 0);
            } else if (buttonFunction == BUTTONSTATE_SELECTWORKINGPOINT) {
                // Set working point manually, move indicator from end to start to let the user click on the desired current
                if (indicatorSetWorkingPointStartTime == 0) {
                    indicatorSetWorkingPointStartTime = currentTime;
                }

                // Decrease of 10% per seconds
                unsigned long elapsed = currentTime - indicatorSetWorkingPointStartTime;
                if (elapsed < 500) {
                    displayedPercentageSetPoint = 100;
                } else if (elapsed > buttonSetWorkingPointFunctionMaxTime - 500) {
                    displayedPercentageSetPoint = 0;
                } else {
                    displayedPercentageSetPoint = 100 - (elapsed - 500) / 100;
                }

                analogWrite(indicatorPin, displayedPercentageSetPoint * percentRatioIndicator);

            } else if (buttonFunction == BUTTONSTATE_AUTOWORKINGPOINT) {
                analogWrite(indicatorPin, 50 * percentRatioIndicator);

            } else if (buttonFunction == BUTTONSTATE_SOFTRESET) {
                analogWrite(indicatorPin, 100 * percentRatioIndicator);
            }
        } else {
            // Execute function
            switch (buttonFunction) {
            case BUTTONSTATE_CURRENT1:
                // Vu-meter read working left position 1
                analogWrite(indicatorPin, constrain((int)(currentF1Average * currentIndicatorRatio), 0, 255));
                break;

            case BUTTONSTATE_CURRENT2:
                // Vu-meter read working left position 2
                analogWrite(indicatorPin, constrain((int)(currentF2Average * currentIndicatorRatio), 0, 255));
                break;

            case BUTTONSTATE_CURRENT3:
                // Vu-meter read working right position 1
                analogWrite(indicatorPin, constrain((int)(currentF3Average * currentIndicatorRatio), 0, 255));
                break;

            case BUTTONSTATE_CURRENT4:
                // Vu-meter read working right position 2
                analogWrite(indicatorPin, constrain((int)(currentF4Average * currentIndicatorRatio), 0, 255));
                break;

            case BUTTONSTATE_DIFFFEED:
                // Change diff feedback state
                dfeedState = !dfeedState;
                setDiffFeed();
                writeEepromTime = currentTime;
                buttonFunction = BUTTONSTATE_NONE;
                break;

            case BUTTONSTATE_SELECTWORKINGPOINT:
                // Set working point
                manualPercentageSetPoint = (byte)displayedPercentageSetPoint;
                autoSetPoint = false;
                buttonFunction = BUTTONSTATE_CURRENT1;
                break;

            case BUTTONSTATE_AUTOWORKINGPOINT:
                // Set working point to auto
                autoSetPoint = true;
                buttonFunction = BUTTONSTATE_CURRENT1;
                break;

            case BUTTONSTATE_SOFTRESET:
                // Set working point
                reset();
                delay(1000);
                sequence = SEQ_DISCHARGE;
                buttonFunction = BUTTONSTATE_NONE;
                break;

            default:
                // BUTTONSTATE_NONE:
                buttonFunction = BUTTONSTATE_NONE;
                analogWrite(indicatorPin, 0);
            }

            indicatorSetWorkingPointStartTime = 0;
        }
    }
}

/*void buttonStateChange() {
  buttonState = digitalRead(buttonPin);
  if (buttonState) {
    buttonLastPressedTime = millis();
  } else {
    buttonLastPressedTime = 0;
  }
  }*/

byte calcPercentSetPoint() {
    if (modulationPeak > 900) {
        return 100;
    }
    if (modulationPeak > 620) {
        return 75;
    }
    if (modulationPeak > 350) {
        return 50;
    }
    if (modulationPeak > 100) {
        return 25;
    }
    return 0;
}

void readEeprom() {
    int position = 0;
    // Read diff feesback
    dfeedState = EEPROM.read(position);
}

void writeEeprom() {
    int position = 0;
    // Add diff feesback
    EEPROM.update(position, dfeedState);
}

void setup() {
    Serial.begin(9600);

    // initialize the digital pin as an output.
    pinMode(ledPin, OUTPUT);
    pinMode(buttonPin, INPUT);
    pinMode(leftDFeedbackPin, OUTPUT);
    pinMode(rightDFeedbackPin, OUTPUT);

    digitalWrite(oneWireBusPin, HIGH);
    digitalWrite(leftDFeedbackPin, HIGH);
    digitalWrite(rightDFeedbackPin, HIGH);

    // initialize Timer1
    noInterrupts(); // disable all interrupts

    // Set PWM speed to the maximum (32KHZ)
    TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
    TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
    TCCR3B = (TCCR3B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
    TCCR4B = (TCCR4B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

    interrupts(); // enable all interrupts

    // attachInterrupt(digitalPinToInterrupt(buttonPin), buttonStateChange, CHANGE);

    reset();

    led.Setup(ledPin, false);

    tempSensors.begin();
    tempSensors.requestTemperatures();
    measureTemperatures(1);

    // Diagnostic
    Serial1.begin(9600);
    dataTxStruct.id = ampId;
    dataTx.begin(details(dataTxStruct), &Serial1);

    analogReference(INTERNAL1V1);

    // Restore values from eprom
    readEeprom();
}

void loop() {
    unsigned int elapsedTime;
    unsigned int check;
    unsigned long currentTime = millis();

    // Update eerpom if necessary
    if (writeEepromTime > 0 && currentTime - writeEepromTime > writeEepromDelay) {
        writeEepromTime = 0;
        writeEeprom();
    }

    if (sequence != SEQ_FAIL) {
        if (airTemp > airTempMax) {
            // Fail, air temperature too high
            sequence = SEQ_FAIL;
            errorNumber = ERR_TEMPTOOHIGH;
            errorCause = ERR_TEMP_AIR;
        } else if (powerSupply1Temp > heatThinkTempMax) {
            // Fail, regulators 1 temperature too high
            sequence = SEQ_FAIL;
            errorNumber = ERR_TEMPTOOHIGH;
            errorCause = ERR_TEMP_PS1;
        } else if (powerSupply2Temp > heatThinkTempMax) {
            // Fail, regulators 2 temperature too high
            sequence = SEQ_FAIL;
            errorNumber = ERR_TEMPTOOHIGH;
            errorCause = ERR_TEMP_PS2;
        } else if (regulatorTemp > heatThinkTempMax) {
            // Fail, regulators 2 temperature too high
            sequence = SEQ_FAIL;
            errorNumber = ERR_TEMPTOOHIGH;
            errorCause = ERR_TEMP_REG;
        } else {
            // Read the input
            unsigned int current1 = analogRead(currentF1Pin);
            unsigned int current2 = analogRead(currentF2Pin);
            unsigned int current3 = analogRead(currentF3Pin);
            unsigned int current4 = analogRead(currentF4Pin);
            currentF1Average += (current1 - currentF1Average) / averageRatio;
            currentF2Average += (current2 - currentF2Average) / averageRatio;
            currentF3Average += (current3 - currentF3Average) / averageRatio;
            currentF4Average += (current4 - currentF4Average) / averageRatio;

            if (sequence >= SEQ_REGULATING) {
                modulation1PeakAverage += (analogRead(modulation1Pin) - (0.132 * max(current1, current2)) - modulation1PeakAverage) / modulationPeakAverageRatio;
                modulation2PeakAverage += (analogRead(modulation2Pin) - (0.132 * max(current3, current4)) - modulation2PeakAverage) / modulationPeakAverageRatio;

                // Calcul du facteur de la mesure de modulation du aux diviseur d'entrï¿½e
                // 33K/(3.3K+51K//51K)=0.116

                // Facteur d'augmentation dans l'enroulement de cathode du transfo
                //=1.33

                if (modulationPeak < modulation1PeakAverage || modulationPeak < modulation2PeakAverage) {
                    sendDatas();

                    // Two overshoot in modulationDetectedMinTime are required to boost the current. Prevent glitch due to a sector pollution.
                    if (currentTime - modulationDetectedTime < modulationDetectedMinTime) {
                        modulationPeak = max(modulation1PeakAverage, modulation2PeakAverage);
                    }

                    modulationDetectedTime = currentTime;
                    if (modulationPeak < modulation1PeakAverage) {
                        modulation1PeakAverage = modulationPeak - 100;
                    }
                    if (modulationPeak < modulation2PeakAverage) {
                        modulation2PeakAverage = modulationPeak - 100;
                    }
                }
            }

            // Calc regulators set point
            unsigned int percentage = autoSetPoint ? calcPercentSetPoint() : manualPercentageSetPoint;
            if (percentageSetPoint != percentage) {
                // New set point must be set
                pidSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent;
                if (sequence >= SEQ_STARTING) {
                    regulate();
                }
                percentageSetPoint = percentage;
            }

            // Average set points for slave regulators from master measure
            pid2SetPoint += (currentF1Average - pid2SetPoint) / slaveAverageRatio;
            pid4SetPoint += (currentF3Average - pid4SetPoint) / slaveAverageRatio;
        }

        displayIndicator();
    }

    switch (sequence) {
    case SEQ_DISCHARGE:
        // Discharging

        // Reset errors
        errorCause = NO_ERR;
        errorNumber = NO_ERR;

        // Pre-sequence
        if (sequenceStartTime == 0) {
            resetRegulators();
            sequenceStartTime = currentTime;
            lastDiagnosticTime = 0;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        led.Execute(800, 200);

        // Diagnostic
        stepMaxTime = dischargeMaxTime;
        stepElapsedTime = elapsedTime;
        stepMaxValue = 0;
        stepCurValue = 1;

        if (elapsedTime > dischargeMaxTime) {
            // Fail, too late
            sequence = SEQ_FAIL;
            errorNumber = ERR_DISHARGETOOLONG;
            break;
        }

        if (elapsedTime < dischargeMinTime || checkInRange(0, startCurrent) == CHECK_RANGE_TOOHIGH) {
            break;
        }

        // Post-sequence
        sendDatas();
        sequenceStartTime = 0;
        sequence++;

    case SEQ_HEAT:
        // Startup tempo

        // Pre-sequence
        if (sequenceStartTime == 0) {
            resetRegulators();
            sequenceStartTime = currentTime;
            lastDiagnosticTime = 0;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        led.Execute(400, 400);

        // Diagnostic
        stepMaxTime = heatMaxTime;
        stepElapsedTime = elapsedTime;
        stepMaxValue = heatMaxTime;
        stepCurValue = elapsedTime;

        if (checkInRange(0, startCurrent + 10) == CHECK_RANGE_TOOHIGH) {
            // Fail, no current allowed now
            sequence = SEQ_FAIL;
            errorNumber = ERR_CURRENTONHEAT;
            break;
        }

        if (elapsedTime < heatMaxTime) {
            break;
        }

        // Diagnostic, force 100%
        stepElapsedTime = heatMaxTime;
        stepCurValue = heatMaxTime;

        // Post-sequence
        sequenceStartTime = 0;
        led.On();
        measureTemperatures(1);
        sendDatas();
        sequence++;
        delay(250);

    case SEQ_STARTING:
        // Starting High Voltage

        // Pre-sequence
        if (sequenceStartTime == 0) {
            relayOn();
            initRegulators();
            sequenceStartTime = currentTime;
            lastDiagnosticTime = 0;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        // Regulation
        computeRegulators();

        led.Execute(20, 400);

        // Diagnostic
        stepMaxTime = highVoltageMaxTime;
        stepElapsedTime = elapsedTime;
        stepMaxValue = 100;
        stepCurValue = calcRegulationProgress(pidSetPoint / 2, pidSetPoint + stabilizedTreshold, pidSetPoint - stabilizedTreshold);

        if (elapsedTime > highVoltageMaxTime) {
            // Fail, too late
            sequence = SEQ_FAIL;
            errorNumber = ERR_STARTINGTOOLONG;
            break;
        }

        if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK) {
            // Fail current error
            sequence = SEQ_FAIL;
            errorNumber = ERR_STARTINGOUTOFRANGE;
            break;
        }

        if (elapsedTime < 5) {
            break;
        }

        // If target points not reached, continue to regulate
        if (checkInRange(pidSetPoint / 2, pidSetPoint + stabilizedTreshold) != CHECK_RANGE_OK) {
            break;
        }

        // Post-sequence
        sequenceStartTime = 0;
        sequence++;

    case SEQ_REGULATING:
        // Waiting for reg

        // Pre-sequence
        if (sequenceStartTime == 0) {
            minCurrent = 0;
            relayOn();
            initRegulators();
            sequenceStartTime = currentTime;
            lastDiagnosticTime = 0;
            elapsedTime = 0;
            setDiffFeed();
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        // Regulation
        computeRegulators();

        led.Execute(20, 1500);

        // Diagnostic
        stepMaxTime = regulationMaxTime;
        stepElapsedTime = elapsedTime;
        stepMaxValue = 100;
        stepCurValue = calcRegulationProgress(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold, stabilizedTreshold - regulationTreshold);

        if (elapsedTime > regulationMaxTime) {
            // Fail, too late
            sequence = SEQ_FAIL;
            errorNumber = ERR_REGULATINGTOOLONG;
            break;
        }

        if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK) {
            // Fail current error
            if (errorTime == 0) {
                errorTime = currentTime;
            }

            if (currentTime - errorTime > errorMaxTime) {
                // Fail current error
                sequence = SEQ_FAIL;
                errorNumber = ERR_REGULATINGMAXREACHED;
                break;
            }
        } else {
            errorTime = 0;
        }

        // If target points not reached, continue to regulate
        if (checkInRange(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold) != CHECK_RANGE_OK) {
            break;
        }

        // Post-sequence
        calcMinCurrent();
        sendDatas();
        sequenceStartTime = 0;
        sequence++;

    case SEQ_FUNCTION:
        // Normal Function

        // Pre-sequence
        if (sequenceStartTime == 0) {
            relayOn();
            initRegulators();
            sequenceStartTime = currentTime;
            lastDiagnosticTime = 0;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        if (dfeedState) {
            led.On();
        } else {
            led.Off();
        }

        // Regulation
        computeRegulators();

        // Measure Temp
        if (currentTime - lastTempMeasureTime > tempMeasureMinTime) {
            measureTemperatures(temperatureAverageRatio);
        }

        // Diagnostic
        stepMaxTime = 0;
        stepElapsedTime = elapsedTime;
        stepMaxValue = 0;
        stepCurValue = 0;

        check = checkInRange(minCurrent, maxCurrent);
        if (check != CHECK_RANGE_OK) {
            if (errorTime == 0) {
                errorTime = currentTime;
            }

            if (currentTime - errorTime > errorMaxTime) {
                // Fail current error
                sequence = SEQ_FAIL;
                errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
                break;
            }
        } else {
            errorTime = 0;
        }

        if (checkInRange(pidSetPoint - functionTreshold, pidSetPoint + functionTreshold) != CHECK_RANGE_OK) {
            if (outOfRangeTime == 0) {
                outOfRangeTime = currentTime;
            }

            if ((currentTime - outOfRangeTime) / 1000 > outOfRangeMaxTime) {
                // Fail out of range error
                sequence = SEQ_FAIL;
                errorNumber = ERR_FUNCTIONOUTOFRANGE;
                break;
            }
        } else {
            outOfRangeTime = 0;
        }
        break;

    default:
        // Fail, protect mode
        relayOff();

        // Diagnostic
        stepMaxTime = 0;
        stepElapsedTime = 0;
        stepMaxValue = 0;
        stepCurValue = 0;
        lastDiagnosticTime = 0;

        led.Execute(250, displayTubeNumber ? errorCause : errorNumber, 1200);
    }

    // Diagnostic
    if (currentTime - lastDiagnosticTime > diagnosticSendMinTime) {
        sendDatas();
    }
}
